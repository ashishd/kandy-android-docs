## Release Notes

## V1.6.08

### General:

* SPiDR SDK
  * Update SDK to version MobileSDK_3.1.1.7b.jar

### New Feature:

* Support Kandy Web Socket

## V1.6.02

### General:

* SPiDR SDK
  * Update SDK to version MobileSDK_3.1.1.5b

### New Features:

* Cloud Storage Services
  * Support file transfer feature
  * Upload file
  * Download file
  * Download file thumbnail
  * Cancel file transfer
