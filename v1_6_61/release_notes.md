# Release Notes

## V1.6.195 - February 26, 2016

### General:

* SPiDR SDK version: MobileSDK_4.0.0.1b
* REST API v1.2

### New Features:

* Missed calls handled fully in KandySDK with push enabled or disabled on domain
* Fixed issues with KandyCallResponseListener
* Added new service : KandyAudioService
 - setAudioDevice(KandyAudioDeviceType audioDeviceType)
 - IKandyAudioDevice getActiveAudioDevice()
 - List<IKandyAudioDevice> getAvailableAudioDevices()
 - registerNotificationListener(KandyAudioServiceNotificationListener listener)
 - unregisterNotificationListener(KandyAudioServiceNotificationListener listener);
* Added IKandyAudioDevice
* Added KandyAudioDeviceType with types : EARPIECE, SPEAKER , HEADPHONES, BLUETOOTH, UNKNOWN
* Added KandyAudioServiceNotificationListener with the callback :
 - onAudioRouteChanged(IKandyAudioDevice activeAudioDevice, KandyError error);
 - onAvailableAudioRoutesChanged(ArrayList<IKandyAudioDevice> availableAudioDevices)
* Added delete api to KandyEventsService
 - deleteHistoryEvents(List<String> eventUUIDs, KandyRecord destination, KandyResponseListener listener);
 - deleteConversations(List<KandyRecord> destinations, KandyResponseListener listener);
 - deleteAllConversations(KandyResponseListener listener);

### Breaking Changes:

* Fixed issues with KandyCallResponseListener

## V1.6.160 - January 15, 2016

### General:

* Update SPiDR SDK to version: 3.1.4.8b
* REST API v1.2

### New Features:

* Add getEventsService to kandy
* Added new API  api to KandyEventsService:

```java
pullHistoryEvents(KandyRecord destination, int numberOfEvents, long timestamp, boolean moveBackword, KandyPullHistoryEventsListner listener);

public void getAllConversations(final KandyAllConversationsListener listener);

public void pullAllConversationsWithMessages(int numberOfEvents, long timestamp, boolean moveBackword, final KandyAllConverstionWithMessagesListener listener);
```

* Added new interface:

```java
IKandyConversation with api :
- getConversationKandyRecord
- getConversationType

IKandySumOfConversation with api:
- getTotalNumOfConversations
- getTotalNumOfGroupsConversations
- getTotalNumOfSingleConversations

KandyConversationType: GROUP or SINGLE
```

* Add ability to set custom logger for Kandy SDK:
* Added new API in KandyLog:

```java
public void setLogger(IKandyLogger logger);
```

* Added IKandyLogger interface:

```java
 public void log(int level, String tag, String message);
 public void log(int level, String tag, String message, Throwable throwable);
```

### Breaking Changes:

* Rename  getEventsHistory  to pullHistoryEvents: in KandyEventsService
* Add parameter - endOfEvents to onResponseSucceded in KandyPendingEventsListner

* Removed static modifier from KandyLog methods:

```java
 public void setLogLevel(int level);
 public int getLogLevel();
```

* Added KandyLog instance access to Kandy for changing logging settings:

```java
 public static KandyLog getKandyLog();
```

## V1.6.88 - November 12, 2015

### General:

* SPiDR SDK
  * Update SPiDR SDK to version: 3.1.4.6b
  * REST API v1.2

### New Features:

* Automatic registration validation with CLI of IVR in incoming call:
* Added new API:

```java
public void requestCode(final KandyValidationMethoud validationMethoud, String destination, String twoLetterISOCountryCode,   String callerPhonePrefix, KandyResponseListener listener);
```

* Support Early Media

### Breaking Changes:

* Added KandyGSMCallServiceNotificationListener class,

```java
 public void onGSMCallIncoming(IKandyCall call, String incomingNumber);
 public void onGSMCallConnected(IKandyCall call, String incomingNumber)
 public void onGSMCallDisconnected(IKandyCall call, String incomingNumber);
```
 * Above methods moved from KandyCallServiceNotificationListener to KandyGSMCallServiceNotificationListener and had changed their signatures.
 * KandyCallServiceNotificationListener extends KandyGSMCallServiceNotificationListener

## V1.6.08

### General:

* SPiDR SDK
  * Update SDK to version MobileSDK_3.1.1.7b.jar

### New Features:

* Support Kandy Web Socket
* Cloud Storage Services
* Support file transfer feature
* Upload file
* Download file
* Download file thumbnail
* Cancel file transfer
