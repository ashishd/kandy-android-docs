## Release Notes

### General:

*   SPiDR SDK

*   Update SDK to version MobileSDK_3.0.5.11b

### New Features:

*   Access

*   Support login with use access token

*   Call Service

*   Support set preferred codec (audio/video) lis
*   Support Early Media for incoming call
*   Support Audio ONLY calls
*   Support inject notification events to SPiDR SDK
*   Support general termination reason base on error code and error message
*   Support make SIP Trunk Calls

### Breaking Changes:

The following syntax changes have been made in this version. Using the old syntax will result in unexpected results, and generate errors.

*   The following methods/properties have changed:

*   KandyProvisioning – rename validate to validateAndProvision
*   KandyCallService – add caller param to createVoipCall and createPSTNCall.
*   KandyCallTerminationReason – change to code and error sting
*   KandyCallState – added SESSION_PROGRESS state to support early media
*   KandyCallServiceNotificationListener – remove onAudioStateChanged use IKandyCall.isMute() instead
*   KandyCallService change createVoipCall API to receive KandyOutgingCallOptions to support media audio only calls
*   KandyCall added isAudioOnlyCall () API KandyCallService change KandyCallTerminationReason call termination reason to error code and error string
